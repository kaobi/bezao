﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class1.Services
{
    public class MathOperations
    {
        public static double MultiplyByTwo(double value) => value * 2;
        
        public static double Square(double value) => value * value;
    }
}
