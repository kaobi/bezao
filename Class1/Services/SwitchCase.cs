﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Class1
{
    class Switch
    {

        public static void AgeGroup()
        {

            Console.WriteLine("Please how old are you?");
            string ageString = Console.ReadLine();
            //ageString = "0";
            int age = int.Parse(ageString);

            switch (age)
            {
                case int aa when aa < 5:
                    Console.WriteLine("you are still a baby");
                    break;
                case int aa when (aa >= 5 && aa < 13):
                    Console.WriteLine("you are just a kid");
                    break;
                case int aa when (aa >= 13 && aa < 20):
                    Console.WriteLine("you are a teenager");
                    break;
                case int aa when (aa >= 20 && aa < 40):
                    Console.WriteLine("you are now a man");
                    break;
                case int aa when (aa >= 40 && aa < 60):
                    Console.WriteLine("you are a matured man");
                    break;
                case int aa when (aa >= 60 && aa < 80):
                    Console.WriteLine("you are an advanced matured man");
                    break;

                default:
                    Console.WriteLine("You are above our age range!!!");
                    break;
            }
        }
    }
}