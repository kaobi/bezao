﻿using Class1.Models;

namespace Class1.Interfaces
{
    public interface IPerson
    {
        Person GetPesronalInformation();

        Person GetAge();

    }
}
