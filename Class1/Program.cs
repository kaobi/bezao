﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Class1.Services;


namespace Class1
{
  
    public class Program
    {

        private delegate double DoubleOp(double y);

        static void Main(string[] args)
        {

            string mid = ", middle part,";
            Func<string, int> stringConcat = (param) =>
            {
                param += mid;
                int returnNumber = 10;
                param += " and this was added to the string.";
                //return Tuple.Create(returnNumber,param);

                return returnNumber;
            };
            Console.WriteLine(stringConcat("Start of string"));

            List<string> Names = new List<string>()
            {
                "Josh",
                "Mike",
                "Kings"
            };

            Names.ForEach(x => Console.WriteLine(x));




            DoubleOp[] operations =
            {
                MathOperations.MultiplyByTwo,
                MathOperations.Square
            };

            Console.ReadLine();


            //for (int i = 0; i <= operations.Length; i++)
            //{
            //    Console.WriteLine($"Using operations[{i}]");
            //    ProcessAndDisplayNumber(operations[i], 2.0);
            //    ProcessAndDisplayNumber(operations[i], 7.3);
            //    ProcessAndDisplayNumber(operations[i], 3.98);
            //    Console.ReadLine();

            //}
        }

        static void ProcessAndDisplayNumber(DoubleOp action, double value)
        {
            double result = action(value);
            Console.WriteLine($"Value is {value}, result of operation is {result} ");

        }

        static string GetMyName(string name)
        {
            string myName = "Nzeh Davison";

            string message = $"The coming {name} is different from the one we typed manually {myName}";
            return message;
        }
    }

}
