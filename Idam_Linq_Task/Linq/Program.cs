﻿using Linq.Services;
using System;

namespace Linq
{
    class Program
    {
        static void Main(string[] args)
        {
            //Implementation.InnerJoin();
            //Implementation.LeftOuterJoin();
            //Implementation.UnionJoin();
            Implementation.Intersect();
            Console.ReadLine();
        }
    }
}
