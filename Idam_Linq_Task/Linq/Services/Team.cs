﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Linq.Services
{
    class Team
    {
        public int T_Id { get; set; }
        public string TeamCode { get; set; }
        public string TeamName { get; set; }
        public int E_id { get; set; }

        public static List<Team> GetTeams()
        {
            return new List<Team>()
            {
            new Team() { T_Id = 1, TeamCode = "EVE", TeamName = "Everton", E_id = 6 },
            new Team() { T_Id = 2, TeamCode = "CHE", TeamName = "Chelsea", E_id = 4 },
            new Team() { T_Id = 3, TeamCode = "ARS", TeamName = "Arsenal", E_id = 3 },
            new Team() { T_Id = 4, TeamCode = "NAP", TeamName = "Napoli", E_id = 5 },
            new Team() { T_Id = 5, TeamCode = "JUV", TeamName = "Juventus", E_id = 2 },
            new Team() { T_Id = 6, TeamCode = "RMD", TeamName = "RealMadrid", E_id = 8 },
            new Team() { T_Id = 7, TeamCode = "BAR", TeamName = "Barcelona", E_id = 1 },
            new Team() { T_Id = 8, TeamCode = "CEL", TeamName = "Celtic", E_id = 7 },
        };
        }
    }
}
