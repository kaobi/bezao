﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Linq.Services
{
    class Implementation
    {
        public static void Leftouterjoin()
        {
      
        }
        public static void InnerJoin()
        {

            var query = from E in Employee.GetEmployees()
                            join T in Team.GetTeams() on E.E_Id equals T.E_id into ET
                                
                        from subcat in ET 
                        select new { EmployeeId = subcat.E_id, EmployeeName = E.EmployeeFullName, Employeemaill = E.EmployeeEmail, TName
                         = subcat.TeamName, TCode = subcat.TeamCode, TeamId = subcat.T_Id};

            var query2 = from ET in query
                         join T in Team.GetTeams() on ET.TeamId equals T.T_Id
                         join E in Employee.GetEmployees() on ET.EmployeeId equals E.E_Id
                         orderby ET.EmployeeId
                         select new
                         {
                             EmployeeId = ET.EmployeeId,
                             EmployeeName = E.EmployeeFullName,
                             Employeemaill = E.EmployeeEmail,
                             TName = T.TeamName,
                             TCode = T.TeamCode,
                         };


            Console.WriteLine("\nThe equivalent operation using InnerJoin():");
                            foreach (var v in query2)
                                Console.WriteLine($"{v.EmployeeId} - {v.EmployeeName} - {v.Employeemaill}  - {v.TName} - {v.TCode}");


        }
        public static void LeftOuterJoin()
        {

            var query = from E in Employee.GetEmployees()
                        join T in Team.GetTeams() on E.E_Id equals T.E_id into ET

                        from subcat in ET.DefaultIfEmpty()
                        select new
                        {
                            EmployeeId = E.E_Id,
                            EmployeeName = E.EmployeeFullName,
                            Employeemaill = E.EmployeeEmail,
                            TName = subcat?.TeamName ?? "Free Agent",
                            TCode = subcat?.TeamCode ?? "No Code for Empty Team"
                         
                        };
            
            Console.WriteLine("\nThe equivalent operation using OuterInnerJoin():");
            foreach (var v in query)
                Console.WriteLine($"{v.EmployeeId} - {v.EmployeeName} - {v.Employeemaill}  - {v.TName} - {v.TCode}");

        }
        public static void UnionJoin()
        {
            Console.WriteLine("\nThe equivalent operation using Union():");
            var query = (from E in Employee.GetEmployees() 
                         select E).Union(Employee.GetEmployees2()).ToList();

            var query2 = Employee.GetEmployees()                            
                            .Union(Employee.GetEmployees2())
                            .ToList();

            foreach (var item in query2)
                Console.WriteLine($"{item.E_Id} - {item.EmployeeFullName} - {item.EmployeeEmail} ");
        }
        public static void Intersect()
        {
            Console.WriteLine("\nThe equivalent operation using Intersect():");
            var query = (from E in Employee.GetEmployees() 
                         select E).Intersect(Employee.GetEmployees2()).ToList();

            var query2 = Employee.GetEmployees().Select(x => x.EmployeeFullName)
                            .Intersect(Employee.GetEmployees2().Select(x => x.EmployeeFullName))
                            .ToList();

            foreach (var item in query2)
                Console.WriteLine($"{item} - ");
        }
    }
}
