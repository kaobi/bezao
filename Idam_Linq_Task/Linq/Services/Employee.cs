﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Linq.Services
{
    class Employee
    {
        public int E_Id { get; set; }
        public string EmployeeFullName { get; set; }
        public string EmployeeEmail { get; set; }

        public static List<Employee> GetEmployees()
        {
            return new List<Employee>()
            {
                new Employee()
                {
                E_Id = 1,
                EmployeeFullName = "Alex Iwobi",
                EmployeeEmail = "alex@gmail.com", /*EmployeeTeam = "Everton",*/
                },
            new Employee()
            {
                E_Id = 2,
                EmployeeFullName = "Ross Barkley",
                EmployeeEmail = "ross@gmail.com", /*EmployeeTeam = "Chelsea",*/
            },
            new Employee()
            {
                E_Id = 3,
                EmployeeFullName = "David Luiz",
                EmployeeEmail = "david@gmail.com", /*EmployeeTeam = "Arsenal",*/
            },
            new Employee()
            {
                E_Id = 4,
                EmployeeFullName = "Victor Oshimen",
                EmployeeEmail = "victor@gmail.com",  /*EmployeeTeam = "Napoli",*/
            },
            new Employee()
            {
                E_Id = 5,
                EmployeeFullName = "Moise Keane",
                EmployeeEmail = "moise@gmail.com", /*EmployeeTeam = "Everton",*/
            },
            new Employee()
            {
                E_Id = 9,
                EmployeeFullName = "Danny Welbeck",
                EmployeeEmail = "danny@gmail.com", /*EmployeeTeam = "Everton",*/
            },
        };

        }
        public static List<Employee> GetEmployees2()
        {
            return new List<Employee>()
            {
                new Employee()
                {
                E_Id = 6,
                EmployeeFullName = "Christain Pulisic",
                EmployeeEmail = "christain@gmail.com", /*EmployeeTeam = "Everton",*/
                },
            new Employee()
            {
                E_Id = 7,
                EmployeeFullName = "Theo Walcolt",
                EmployeeEmail = "theo@gmail.com", /*EmployeeTeam = "Chelsea",*/
            },
            new Employee()
            {
                E_Id = 8,
                EmployeeFullName = "Nathan Ake",
                EmployeeEmail = "nathan@gmail.com", /*EmployeeTeam = "Arsenal",*/
            },
            new Employee()
            {
                E_Id = 10,
                EmployeeFullName = "Victor Moses",
                EmployeeEmail = "victor@gmail.com",  /*EmployeeTeam = "Napoli",*/
            },
            new Employee()
            {
                E_Id = 11,
                EmployeeFullName = "Jordan Ibe",
                EmployeeEmail = "jordan@gmail.com", /*EmployeeTeam = "Everton",*/
            },
            new Employee()
            {
                E_Id = 12,
                EmployeeFullName = "Gary Cahill",
                EmployeeEmail = "gary@gmail.com", /*EmployeeTeam = "Everton",*/
            },
            new Employee()
                {
                E_Id = 1,
                EmployeeFullName = "Alex Iwobi",
                EmployeeEmail = "alex@gmail.com", /*EmployeeTeam = "Everton",*/
                },
            new Employee()
            {
                E_Id = 2,
                EmployeeFullName = "Ross Barkley",
                EmployeeEmail = "ross@gmail.com", /*EmployeeTeam = "Chelsea",*/
            },
            new Employee()
            {
                E_Id = 3,
                EmployeeFullName = "David Luiz",
                EmployeeEmail = "david@gmail.com", /*EmployeeTeam = "Arsenal",*/
            },
        };
        }
    }
}
