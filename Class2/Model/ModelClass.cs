﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Class2.Model
{
    public class ModelClass
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OtherName { get; set; }
        public int Age { get; set; }

    }
}