﻿using CollectionsAndLinqQuery.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsAndLinqQuery.ClassExtension
{
    public static class RiderExtension
    {
        public static void PrintRiders(this Riders riders)
        {
            Console.WriteLine($"{riders.FirstName} {riders.LastName}");
        }
    }
}
