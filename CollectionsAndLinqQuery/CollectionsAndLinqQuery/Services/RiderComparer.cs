﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsAndLinqQuery.Services
{
    class RiderComparer : IComparer<Riders>
    {
        private readonly CompareType compareType;
        public enum CompareType
        {
            FirstName,
            LastName,
            Country,
            Wins
        }

        public RiderComparer(CompareType _compareType)
        {
            compareType = _compareType;
        }
        public int Compare(Riders x, Riders y)
        {
            if (x == null && y == null) return 0;
            if (x == null) return -1;
            if (y == null) return 1;

            int res;

            switch (compareType)
            {
                case CompareType.FirstName:
                    return string.Compare(x.FirstName, y.FirstName);
                    
                case CompareType.LastName:
                    return string.Compare(x.LastName, y.LastName);                    

                default:
                    return -2;
            }
        }
    }
}
