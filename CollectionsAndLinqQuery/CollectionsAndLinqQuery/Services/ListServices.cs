﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CollectionsAndLinqQuery.Services
{
    public class ListServices
    {
        public static void ListUsage()
        {
            //Declaration
            var intList = new List<int>() { 1, 2 };

            var list = new List<decimal>();

            var stringList = new List<string>() { "one", "two" };

            //Adding El
            intList.Add(1);
            intList.Add(2);

            stringList.Add("one");
            stringList.Add("two");

            //var graham = new Riders(7, "Graham", "Hill", "UK", 14);
            //var emerson = new Riders(13, "Emerson", "Fittipaldi", "Brazil", 14);
            //var mario = new Riders(16, "Mario", "Andretti", "USA", 12);

            //var riders = new List<Riders>(20) { graham, emerson, mario };

            ////adding object model to list
            //riders.Add(new Riders(24, "Michael", "Schumacher", "Germany", 91));
            //riders.Add(new Riders(27, "Mika", "Hakkinen", "Finland", 20));

            ////Add Range to a list
            //riders.AddRange(new Riders[] {
            //new Riders(14, "Niki", "Lauda", "Austria", 25),
            //new Riders(21, "Alain", "Prost", "France", 51)});

            //var ridersList = new List<Riders>(
            // new Riders[] {
            // new Riders(12, "Jochen", "Rindt", "Austria", 6),
            // new Riders(22, "Ayrton", "Senna", "Brazil", 41) });

            ////inserting element
            //ridersList.Insert(3, new Riders(6, "Phil", "Hill", "USA", 3));


            //Searching 
            //int index1 = ridersList.IndexOf(mario); //returns -1 if no match exist

            //riders.IndexOf(mario);

            //riders.FindIndex(x => x.FirstName == "mario");

            //riders.Sort(new RiderComparer(RiderComparer.CompareType.LastName));

            //sort 
           // riders.Sort((r1, r2) => r2.Wins.CompareTo(r1.Wins));

           // riders.ForEach(r =>
           //{
           //    Console.WriteLine($"{r.FirstName} {r.Wins}");

           //});


            //FIFO
            Queue<int> queue = new Queue<int>(); //eg bank queue


            //LIFO
            var stack = new Stack<string>(); //eg sliced bread

            LinkedList<string> linkedList = new LinkedList<string>(); //eg twitter trends

            Dictionary<string, Riders> dictRiders = new Dictionary<string, Riders>();
           

            //dictRiders.Add("USA", mario);
            //dictRiders["Naija"] = graham;

            
            //ILookup<int, Riders> scoreList = riders.ToLookup(c => c.Wins);

            HashSet<string> hset = new HashSet<string>();

            SortedSet<string> sset = new SortedSet<string>();

            

            Console.ReadLine();
        }
    }
}
