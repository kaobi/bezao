﻿using System;
using System.Collections.Generic;

namespace CollectionsAndLinqQuery.Services
{
    public class Riders : IComparable<Riders>, IFormattable
    {
        //public int Id { get; }
        public string FirstName { get; }
        public string LastName { get; }
        public string Country { get; }
        public int Wins { get; }
        public int Starts { get; }
        public IEnumerable<int> Years { get; }
        public IEnumerable<string> Cars { get; }

        public Riders(string firstName, string lastName, string country, int start, int wins)
            : this(firstName, lastName, country, wins, start, null, null)
        { }

        public Riders(string firstName, string lastName, string country, int start, int wins, IEnumerable<int> years, IEnumerable<string> cars)
        {
            FirstName = firstName;
            LastName = lastName;
            Country = country;
            Wins = wins;
            Starts = start;
            Years = years != null ? new List<int>(years) : new List<int>();
            Cars = cars != null ? new List<string>(cars) : new List<string>();
        }

        public override string ToString() => $"{FirstName} {LastName}";


        public string ToString(string format, IFormatProvider formatProvider)
        {
            switch (format)
            {
                case null:
                case "N":
                    return ToString();
                case "F":
                    return FirstName;
                case "L":
                    return LastName;
                case "C":
                    return Country;
                case "S":
                    return Starts.ToString();
                case "W":
                    return Wins.ToString();
                case "A":
                    return $"{FirstName} {LastName}, {Country}; starts: {Starts}, wins: {Wins}";
                default:
                    throw new FormatException($"Format {format} not supported");
            }
        }

        public int CompareTo(Riders other)
        {
            int compare = LastName?.CompareTo(other?.LastName) ?? -1;
            if (compare == 0)
            {
                return FirstName?.CompareTo(other?.FirstName) ?? -1;
            }
            return compare;
        }
    }
}
