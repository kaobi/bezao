﻿using CollectionsAndLinqQuery.Model;
using System;
using System.Linq;

namespace CollectionsAndLinqQuery.Services
{
    class GroupSample
    {
        static void Grouping()
        {
            var countries = from r in FormulaOne.GetChampions()
                            group r by r.Country into g
                            orderby g.Count() descending, g.Key
                            where g.Count() >= 2
                            select new
                            {
                                Country = g.Key,
                                Count = g.Count()
                            };

            foreach (var c in countries)
            {
                Console.WriteLine($"{c.Country}: {c.Count}"); 
            }

        }

        static void GroupingWithMethods()
        {
            var countries = FormulaOne.GetChampions()
                             .GroupBy(r => r.Country)
                             .OrderByDescending(r => r.Count())
                             .ThenBy(r => r.Key)
                             .Where(r => r.Count() >= 2)
                             .Select(x =>
                                new
                                {
                                    Country = x.Key,
                                    Count = x.Count()
                                }
                             );
                
        }

        static void GroupingWithVariables()
        {
            var countries = from r in FormulaOne.GetChampions()
                            group r by r.Country into g
                            let count = g.Count()
                            orderby count descending, g.Key
                            where count >= 2
                            select new
                            {
                                Country = g.Key,
                                Count = count
                            };

            foreach (var c in countries)
            {
                Console.WriteLine($"{c.Country}: {c.Count}");
            }
        }

        static void GroupingWithAnonymousTypes()
        {
            var countries = FormulaOne.GetChampions()
                            .GroupBy(r => r.Country)
                            .Select(g => new { Group = g, Count = g.Count() })
                            .OrderByDescending(g => g.Count)
                            .ThenBy(g => g.Group.Key)
                            .Where(g => g.Count >= 2)
                            .Select(g => new
                            {
                                Country = g.Group.Key,
                                Count = g.Count
                            });

            foreach (var c in countries)
            {
                Console.WriteLine($"{c.Country}: {c.Count}");
            }
        }

        static void GroupingAndNestedObjects()
        {
            var countries = from r in FormulaOne.GetChampions()
                            group r by r.Country into g
                            let count = g.Count()
                            orderby count descending, g.Key
                            where count >= 2
                            select new
                            {
                                Country = g.Key,
                                Count = count,
                                Racers = from r1 in g
                                         orderby r1.LastName
                                         select r1.FirstName + " " + r1.LastName
                            };
            foreach (var item in countries)
            {
                Console.WriteLine($"{item.Country,-10} {item.Count}");
                foreach (var name in item.Racers)
                {
                    Console.Write($"{name}; ");
                }
                Console.WriteLine();
            }
        }
    }
}
