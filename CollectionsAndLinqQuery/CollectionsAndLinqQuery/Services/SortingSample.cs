﻿using CollectionsAndLinqQuery.Model;
using System;
using System.Linq;

namespace CollectionsAndLinqQuery.Services
{
    class SortingSample
    {
        public static void SortMultiple()
        {
            var racers = (from r in FormulaOne.GetChampions()
                          orderby r.Country, r.LastName, r.FirstName
                          select r).Take(5);

            foreach (Riders r in racers)
            {
                Console.WriteLine($"{r.Country}: {r.LastName}, {r.FirstName } ");
            }
        }

        public static void SortMultipleWithMethods()
        {
            var racers = FormulaOne.GetChampions()
                            .OrderBy(r => r.Country)
                            .ThenBy(r => r.LastName)
                            .ThenBy(r => r.FirstName)
                            .Take(5);

            foreach (Riders r in racers)
            {
                Console.WriteLine($"{r.Country}: {r.LastName}, {r.FirstName } ");
            }
        }
    }
}

