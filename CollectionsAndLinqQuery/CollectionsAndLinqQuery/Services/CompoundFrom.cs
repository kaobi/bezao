﻿using CollectionsAndLinqQuery.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsAndLinqQuery.Services
{
    class CompoundFrom
    {
        public static void CompoundFromExample()
        {
            var ferrariDrivers = from r in FormulaOne.GetChampions()
                                 from c in r.Cars
                                 where c == "Ferrari"
                                 orderby r.LastName
                                 select r.FirstName + " " + r.LastName;

            foreach (var item in ferrariDrivers)
            {
                Console.WriteLine(item);
            }
        }

        public static void CompoundFromWithMethods()
        {
            var ferrariDrivers = FormulaOne.GetChampions()
                                    .SelectMany(e => e.Cars, (r, c) => new { Racer = r, Car = c })
                                    .Where(e => e.Car == "Ferrari")
                                    .OrderBy(e => e.Racer.LastName)
                                    .Select(e => e.Racer.FirstName + " " + e.Racer.LastName);


            foreach (var item in ferrariDrivers)
            {
                Console.WriteLine(item);
            }

        }
    }
}
