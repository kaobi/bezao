﻿using CollectionsAndLinqQuery.ClassExtension;
using CollectionsAndLinqQuery.Model;
using CollectionsAndLinqQuery.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsAndLinqQuery
{
    class Program
    {
        static void Main(string[] args)
        {
            //List example
            //ListServices.ListUsage();

            //Linq examole
            LinqExample();

            //using Lamda
            //LinqUsingLamda();

            //Deferred Query
            //DeferredQuery();

            //UnDeferred Query
            //UnDeferredQuery();

            //Multiple sorting 
            //SortingSample.SortMultiple();

            //Console.WriteLine("\n With method \n");

            ////Multiple sorting with method
            //SortingSample.SortMultipleWithMethods();


            //Compound from Example 
            //CompoundFrom.CompoundFromExample();

            //Console.WriteLine("\n With method \n");

            ////Compound from Example with method
            //CompoundFrom.CompoundFromWithMethods();

            Console.ReadLine();
        }

        static void LinqExample()
        {
            var query = from r in FormulaOne.GetChampions()
                        where r.Country == "Italy"
                        orderby r.Wins descending
                        select r;

            foreach (Riders r in query)
            {
                r.PrintRiders();
            }

            Console.WriteLine("End of Linq \n");
        }

        static void LinqUsingLamda()
        {
            var res = FormulaOne.GetChampions()
                        .Where(e => e.Country == "Italy")
                        .OrderByDescending(e => e.Wins)
                        .Select(x => x);

            Console.WriteLine("Using Lamda \n");

            foreach (Riders r in res)
            {
                Console.WriteLine($"{r:A}");

            }
        }

        static void DeferredQuery()
        {
            var names = new List<string> { "Nino", "Alberto", "Juan", "Mike", "Phil" };
            var namesWithJ = from n in names
                             where n.StartsWith("J")
                             orderby n
                             select n;

            Console.WriteLine("First iteration");
            foreach (string name in namesWithJ)
            {
                Console.WriteLine(name);
            }
            Console.WriteLine();

            names.Add("John");
            names.Add("Jim");
            names.Add("Jack");
            names.Add("Denny");
            Console.WriteLine("Second iteration \n");
            foreach (string name in namesWithJ)
            {
                Console.WriteLine(name);
            }
        }

        static void UnDeferredQuery()
        {
            var names = new List<string> { "Nino", "Alberto", "Juan", "Mike", "Phil" };
            var namesWithJ = (from n in names
                              where n.StartsWith("J")
                              orderby n
                              select n).ToList();

            Console.WriteLine("First iteration \n");
            foreach (string name in namesWithJ)
            {
                Console.WriteLine(name);
            }
            Console.WriteLine();

            names.Add("John");
            names.Add("Jim");
            names.Add("Jack");
            names.Add("Denny");
            Console.WriteLine("Second iteration");
            foreach (string name in namesWithJ)
            {
                Console.WriteLine(name);
            }
        }
    }
}
