﻿using EventsAndEventHandlers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsAndEventHandlers.Services
{
    public class CustomerService
    {
        private string customerName;
        public CustomerService(string _customerName) => customerName = _customerName;

        public void NewCarIsHere(object sender, CarInfoArgs e)
        {
            
            Console.WriteLine($"{customerName}: car {e.Car.Name} with Model Number {e.Car.ModelNumber} is new, was made on {e.Car.Date.Value.ToLongDateString()} ");
        }
        
    }
}
