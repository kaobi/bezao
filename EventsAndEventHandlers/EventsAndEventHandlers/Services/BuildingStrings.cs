﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsAndEventHandlers.Services
{
    /// <summary>
    /// 
    /// 
    /// string are immutable data type - after you initialize a string object, that string object can never change
    /// </summary>
    public class BuildingStrings
    {

        public static void example()
        {
            //string greetingText = "Hello from all the people at Wrox Press. ";
            //greetingText += "We do hope you enjoy this book as much as we enjoyed writing it.";

            //greetingText.Replace(' ', '_');
            //char a = ' ';
            //StringBuilder dd;

            //for (int i = 'z'; i >= 'a'; i--)
            //{
            //    char old1 = (char)i;
            //    char new1 = (char)(i + 1);
            //    greetingText = greetingText.Replace(old1, new1);
            //}

            //for (int i = 'Z'; i >= 'A'; i--)
            //{
            //    char old1 = (char)i;
            //    char new1 = (char)(i + 1);
            //    greetingText = greetingText.Replace(old1, new1);
            //}
            //Console.WriteLine($"Encoded:\n {greetingText}");

            var sb = new StringBuilder();

            List<string> subjects = new List<string>()
            {
                "Maths",
                "Physics",
                "English",
                "Biology",
                "Chemistry",
                "Igbo",
                "Government"
            };

            sb.Append("A science student is to take the following subjects:");

            subjects.ForEach(s =>
            {
                if(s != "Government")
                {
                    sb.AppendLine(s);
                                      
                }
            });
            
            string.Format($"A science student is to take the following subject {{fdaadasdasd}} hbakjdhaj kbkshdkjbn");
            string.Format("A science student is to take the following subject \n fdaadasdasd \t hbakjdhaj kbkshdkjbn");
                        
            Console.WriteLine(sb);
            Console.ReadLine();

            string sa = "My is faaaa" + "i am fafafaf" + "i am from jjjjjjj ";

            string month = "",
                year = "",
                staffType = "aaaa";

            //string concatenation
            String.Format($"select distinct MDA from LegacyOctoberAutoPay where Paymentmonth = '" + month + "' and Paymentyear = '" + year + "' And StaffType = '" + staffType + "' order by 1");
          
            //string interpolation
            String.Format($"select distinct MDA from LegacyOctoberAutoPay where Paymentmonth = '{month}' and Paymentyear = '{year}' And StaffType = '{staffType}' order by 1");

        }
    }
}
