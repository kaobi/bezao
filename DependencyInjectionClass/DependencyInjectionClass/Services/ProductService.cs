﻿using DependencyInjectionClass.Entities;
using DependencyInjectionClass.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DependencyInjectionClass.Services
{
    public class ProductService : IProduct
    {
        public IEnumerable<Product> GetAll()
        {
            var products = new List<Product>()
            {
                new Product {
                    Id = 1,
                    Name = "Milk" ,
                    Price = 250,
                    Quantity = 100
                } ,
                new Product
                {
                    Id = 1,
                    Name = "Milo",
                    Price = 75,
                    Quantity = 150
                }
            };

            return products;
        }

        public Product GetOne(int Id)
        {
            return GetAll().Where(x => x.Id == Id).FirstOrDefault();
        }

        public bool isInStock(int Id)
        {
            return GetAll().Any(x => x.Id == Id && x.Quantity > 0);
        }
    }
}