﻿using DependencyInjectionClass.Entities;
using DependencyInjectionClass.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DependencyInjectionClass.Services
{
    public class ProductServiceWithDb : IProduct
    {
        ApplicationContext context;
        public ProductServiceWithDb(ApplicationContext appContext)
        {
            context = appContext;
        }
        
        public IEnumerable<Product> GetAll()
        {
            return context.Products.AsQueryable();
        }

        public Product GetOne(int Id)
        {
            return context.Products.FirstOrDefault(x => x.Id == Id);
        }

        public bool isInStock(int Id)
        {
            return context.Products.Any(x => x.Id == Id && x.Quantity > 0);
        }
    }
}