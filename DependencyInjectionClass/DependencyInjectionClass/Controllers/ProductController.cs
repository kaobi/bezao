﻿using DependencyInjectionClass.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DependencyInjectionClass.Controllers
{
    public class ProductController : Controller
    {
        IProduct _productRepo;

        public ProductController(IProduct productRepo)
        {
            _productRepo = productRepo;
        }

        // GET: Product
        public ActionResult Index()
        {
            var productList = _productRepo.GetAll();

            return View(productList);
        }

        public ActionResult Details(int? Id)
        {
            try
            {
                if(Id == 0)
                {
                    ViewBag.Error = "Invalid Request sent, Try Again";
                    return View();
                }

                var product = _productRepo.GetOne(Id.Value);
                return View(product);

            }
            catch (Exception ex)
            {

                ViewBag.Error =  $"An Error Occured: {ex.Message}";
            }

            return View();
        }
    }
}