﻿using DependencyInjectionClass.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInjectionClass.Interfaces
{
    public interface IProduct
    {
        IEnumerable<Product> GetAll();
        Product GetOne(int Id);
        bool isInStock(int Id);
    }
}
