﻿using DependencyInjectionClass.Entities;
using DependencyInjectionClass.Interfaces;
using DependencyInjectionClass.Services;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DependencyInjectionClass.Infrastructure
{
    public class NinjectResolver : IDependencyResolver
    {
        private IKernel _kernel;
        public NinjectResolver(IKernel kernel)
        {
            _kernel = kernel;
            AddBindings();
        }

        private void AddBindings()
        {
            _kernel.Bind<IProduct>().To<ProductServiceWithDb>();
            _kernel.Bind<ApplicationContext>().ToSelf();
        }

        public object GetService(Type serviceType) => _kernel.TryGet(serviceType);

        public IEnumerable<object> GetServices(Type serviceType) => _kernel.GetAll(serviceType);
    }
}