﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1.Helpers
{
    public static class DateTimeOffsetExtension
    {
        public static string CalculateAge(this DateTimeOffset dateTimeOffset)
        {

            var currentDate = DateTime.UtcNow;
            int age = currentDate.Year - dateTimeOffset.Year;

            if (currentDate < dateTimeOffset.AddYears(age))
            {
                age--;
            }

            return age.ToString();
        }
    }
}
