﻿using System;
using Task1.Helpers;

namespace Task1.Services
{
    public static class InfoService
    {
        public static string GetName()
        {
            Console.WriteLine("Please Enter your Name");

            string name = Console.ReadLine();
            if (string.IsNullOrEmpty(name))
            {
                Console.WriteLine("You are required to Enter your Name");
            }

            Console.WriteLine(".......");

            return name;
        }

        public static string GetAge()
        {
            Console.WriteLine("Enter your date of birth");

            string date = Console.ReadLine();

            string age = "";

            try
            {
                var currentDate = DateTimeOffset.Parse(date);

                age = currentDate.CalculateAge();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            

            return age;
        }


    }
}
