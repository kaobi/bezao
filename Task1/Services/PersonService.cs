﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task1.Models;

namespace Task1.Services
{
    public class PersonService
    {
        private static Person person;
        public PersonService(string fname, string lastName, string age)
        {
            person = new Person()
            {
                FirstName = fname,
                LastName = lastName,
                Age = age
            };
        }

        public static void GetPersonalInfo()
        {
            Console.WriteLine($"My fullname is {person.LastName} {person.FirstName} and i am {person.Age} years old");
        }
    }
}
